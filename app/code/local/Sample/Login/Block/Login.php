<?php

/**
 * Class Sample_Contact_Block_Block
 */
class Sample_Login_Block_Login extends Mage_Core_Block_Template
{

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('loginOne.phtml');
    }

    /**
     * @return string
     */
    public function getMessage()
    {
       return ' avant login';
    }

    public function getLogin()
    {
        return Mage::registry('login');
    }

}