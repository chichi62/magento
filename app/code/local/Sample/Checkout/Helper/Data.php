<?php

/**
 * Class Sample_Checkout_Helper_Data
 */
class Sample_Checkout_Helper_Data extends Mage_Checkout_Helper_Data
{
    /**
     * Check is allowed Guest Checkout
     * Use config settings and observer
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isAllowedGuestCheckout(Mage_Sales_Model_Quote $quote, $store = null)
    {
        return false;
    }
}
