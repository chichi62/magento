<?php

class Sample_Premiumshipment_Model_Carrier
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{

    protected $_code = 'sample_premiumshipment';


    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        $result = Mage::getModel('shipping/rate_result');
        /* @var $result Mage_Shipping_Model_Rate_Result */

        $result->append($this->_getStandardShippingRate());

        return $result;
    }

    protected function _getStandardShippingRate(){
        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */

        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));

        $rate->setMethod($this->_code);
        $rate->setMethodTitle($this->getConfigData('title'));

        $weightTotal = 0;

        $quote = Mage::getModel('checkout/session')->getQuote();
        /* @var $quote Mage_Sales_Model_Quote */

        $products = $quote->getItemsCollection();
         /* @var $products Mage_Eav_Model_Entity_Collection_Abstract */
         foreach($products as $product)
         {
             /* @var $product Mage_Sales_Model_Quote_Item */
             //Mage::log($product->getWeight() . "qty => " . $product->getQty(), null, 'quote.log', true);
             $weightTotal += $product->getWeight() * $product->getQty();
         }

        //Mage::log($weightTotal, null, 'quote.log', true);

         if($weightTotal > 1.00)
         {
             $rate->setPrice(40);
         }else{
             $rate->setPrice($this->getConfigData('price'));
         }

        return $rate;
    }

    public function getAllowedMethods()
    {
        return array($this->_code => $this->getConfigData('title'));
    }

}
