<?php

/**
 * Class Sample_Catalog_Block_Product_List
 */
class Sample_Catalog_Block_Product_List extends Mage_Catalog_Block_Product_List
{

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        parent::_getProductCollection();
        return $this->_productCollection->setPageSize(4);
    }

}