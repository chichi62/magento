<?php

/** @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$attribute_code = 'provider';

if(!($this instanceof Mage_Eav_Model_Entity_Setup) )
{
    throw new Exception("Resource Class needs to inherit from " .
        "Mage_Catalog_Model_Resource_Setup for this to work");
}

$installer->removeAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    $attribute_code
);

$attr = array (
    'attribute_model' => NULL,
    'backend' => '',
    'type' => 'int',
    'table' => '',
    'frontend' => '',
    'input' => 'select',
    'label' => 'Fournisseurs',
    'frontend_class' => '',
    'source' => '',
    'required' => '0',
    'user_defined' => '1',
    'default' => '',
    'unique' => '0',
    'note' => '',
    'input_renderer' => NULL,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'group' => 'General',
    'visible' => '1',
    'searchable' => '1',
    'filterable' => '1',
    'comparable' => '1',
    'visible_on_front' => '0',
    'is_html_allowed_on_front' => '0',
    'is_used_for_price_rules' => '1',
    'filterable_in_search' => '1',
    'used_in_product_listing' => '1',
    'used_for_sort_by' => '1',
    'is_configurable' => '1',
    'apply_to' => 'simple',
    'visible_in_advanced_search' => '1',
    'position' => '1',
    'wysiwyg_enabled' => '0',
    'used_for_promo_rules' => '1',
    'option' =>
        array (
            'values' =>
                array (
                    0 => 'LISAGRI',
                    1 => 'GOFAQ',
                    2 => 'GEDIMAT',
                    3 => 'BRICO',
                    4 => 'BRICOZOR',
                    5 => 'RIOBY'
                ),
        ),
);

$this->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attribute_code, $attr);


$installer->endSetup();
