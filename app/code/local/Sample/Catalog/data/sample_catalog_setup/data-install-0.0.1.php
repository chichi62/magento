<?php

/** @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

if(!($this instanceof Mage_Eav_Model_Entity_Setup) )
{
    throw new Exception("Resource Class needs to inherit from " .
        "Mage_Catalog_Model_Resource_Setup for this to work");
}

// recuperer l'attribut créer précédemment
$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'provider');
/* @var $attribute Mage_Eav_Model_Entity_Attribute_Abstract */

// recupere son l'id de l'option "BRICO"
$optionId = $attribute->getSource()->getOptionId("BRICO");


// 1 produit aleatoire
$collection = Mage::getModel('catalog/product')->getCollection();
$collection->addAttributeToSelect('*');
$collection->addAttributeToSelect('provider');
$collection->getSelect()->order('RAND()');
$collection->getSelect()->limit(1);


foreach($collection as $product)
{
    $product->setData('provider', $optionId);
    $product->getResource()->saveAttribute($product, 'provider');
}

$installer->endSetup();
