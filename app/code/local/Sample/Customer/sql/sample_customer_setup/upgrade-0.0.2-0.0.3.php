<?php

$installer = $this;
/* @var $installer Mage_Customer_Model_Resource_Setup */

$installer->startSetup();

if(!($this instanceof Mage_Eav_Model_Entity_Setup) )
{
    throw new Exception("Resource Class needs to inherit from " .
        "Mage_Customer_Model_Resource_Setup for this to work");
}

$attribute_code = 'pseudo';
$entityTypeId = 'customer';
$installer->removeAttribute($entityTypeId,$attribute_code);
$installer->addAttribute($entityTypeId, $attribute_code, array(
    'type' => 'varchar',
    'label' => 'Pseudo',
    'input' => 'text',
    'visible' => TRUE,
    'required' => TRUE,
));

$attribute = Mage::getSingleton('eav/config')->getAttribute($entityTypeId, $attribute_code);
$attribute->setData('used_in_forms', array(
    'adminhtml_customer',
    'adminhtml_checkout',
    'adminhtml_customer_address',
    'checkout_account_create',
    'checkout_register',
    'customer_account_create',
    'customer_account_edit',
    'customer_address_edit',
    'customer_register_address'
));

$attribute->save();

$installer->endSetup();