<?php

$installer = $this;
/* @var $installer Mage_Customer_Model_Resource_Setup */

$installer->startSetup();


$installer->getConnection()->addColumn($installer->getTable('sales_flat_order_address'), 'pseudo', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => false,
    'length' => 255,
    'comment' => 'Pseudo'
));

$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_address'), 'pseudo', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => false,
    'length' => 255,
    'comment' => 'Pseudo'
));

$installer->endSetup();

