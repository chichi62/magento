<?php

/**
 * Class Sample_Customer_Model_Observer
 */
class Sample_Customer_Model_Observer
{
    /**
     * @return $this
     */
    public function birthdayAlert()
    {
        $customerCollection = Mage::getModel('customer/customer')->getCollection()
            ->addFieldToFilter('dob', date('Y-m-d'));

        foreach($customerCollection as $customer){
            $this->_sendBirthdayEmail($customer);
        }

        return $this;
    }


    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return bool
     */
    protected function _sendBirthdayEmail(Mage_Customer_Model_Customer $customer)
    {
        Mage::log('_sendBirthdayEmail', null, 'sample_customer.log', true);

        //return mail('jkelle@atecna.fr', 'Mon Sujet', 'coucou');


        // 1) email transactionnel faire une partie system config
        // recup avec Mage::getConfig()
        // 2) email html dans locale
        // recup le nom declarer dans le config.xml
        $template = 'sample_customer_email_happyday';

        /* @var $emailTemplate Mage_Core_Model_Email_Template */
        $emailTemplate = Mage::getModel('core/email_template');

        $sender = array(
            'name' => 'Kelle',
            'email' => 'jocelynkelle@gmail.com'
        );

        try{
            $emailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => Mage::app()->getStore()->getId()))
                          ->sendTransactional(
                              $template,
                              $sender,
                              'jkelle@atecna.fr',
                              'jocelyn',
                              array('customer' => $customer)
                          );
        }catch(Exception $e){
            Mage::logException($e);
        }

        return $emailTemplate;

    }
}
