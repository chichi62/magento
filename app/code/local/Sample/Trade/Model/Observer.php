<?php

/**
 * Class Sample_Trade_Model_Observer
 */
class Sample_Trade_Model_Observer
{

    public function addProduct(Varien_Event_Observer $obs)
    {
        $helper = Mage::helper('sample_trade');
        $quote = Mage::helper('checkout/cart')->getCart()->getQuote();
        $itemDiscounted = false;
        $amountCategory = 0;

        foreach ($quote->getAllItems() as $item) {
            // on regarde si le produit est present dans le panier
            if ($item->getProductId() == $helper->getProductId()) {
                $itemDiscounted = $item;
            }

            $priceProduct = $item->getPrice();
            $qty = $item->getQty();
            $amountProduct = $priceProduct * $qty;

            $varienProduct = new Varien_Object();
            $varienProduct->setId($item->getProductId());
            $productCategoryIds = Mage::getResourceModel('catalog/product')->getCategoryIds($varienProduct);

            foreach($productCategoryIds as $productCategoryId) {
                if ($productCategoryId == $helper->getCategory()) {
                    $amountCategory = $amountCategory + $amountProduct;
                }
            }
        }

        if($amountCategory > $helper->getAmount()){
            $itemDiscounted = ($itemDiscounted->getParentItem() ? $itemDiscounted->getParentItem() : $itemDiscounted);
            $price = $this->_getPriceByItem($itemDiscounted);
            // Set the custom price
            $itemDiscounted->setCustomPrice($price);
            $itemDiscounted->setOriginalCustomPrice($price);
            // Enable super mode on the product.
            $itemDiscounted->getProduct()->setIsSuperMode(true);
        }

    }

    public function updateShippingCart(Varien_Event_Observer $obs)
    {
        $helper = Mage::helper('sample_trade');
        $quote = $obs->getEvent()->getCart()->getQuote();
        $itemDiscounted = false;
        $amountCategory = 0;

        foreach ($quote->getAllItems() as $item) {
            // on regarde si le produit est present dans le panier
            if ($item->getProductId() == $helper->getProductId()) {
                $itemDiscounted = $item;
            }

            $priceProduct = $item->getPrice();
            $qty = $item->getQty();
            $amountProduct = $priceProduct * $qty;
            Mage::log($item->getName() .' name ' . $priceProduct . ' price ' . $qty . ' qty ', null, 'ffff.log', true);

            $varienProduct = new Varien_Object();
            $varienProduct->setId($item->getProductId());
            $productCategoryIds = Mage::getResourceModel('catalog/product')->getCategoryIds($varienProduct);

            foreach($productCategoryIds as $productCategoryId) {
                if ($productCategoryId == $helper->getCategory()) {
                    $amountCategory = $amountCategory + $amountProduct;
                }
            }
        }

       // Mage::log($amountCategory, null, 'ffff.log', true);exit;

        if($amountCategory > $helper->getAmount()){
            $itemDiscounted = ($itemDiscounted->getParentItem() ? $itemDiscounted->getParentItem() : $itemDiscounted);
            $price = $this->_getPriceByItem($itemDiscounted);
            // Set the custom price
            $itemDiscounted->setCustomPrice($price);
            $itemDiscounted->setOriginalCustomPrice($price);
            // Enable super mode on the product.
            $itemDiscounted->getProduct()->setIsSuperMode(true);
        }
    }



    protected function _getPriceByItem(Mage_Sales_Model_Quote_Item $item)
    {
        $helper = Mage::helper('sample_trade');
        return $helper->getNewAmountProduct();
    }

}