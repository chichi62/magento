<?php


/**
 * Class Sample_Trade_Model_System_Config_Source_Product
 */
class Sample_Trade_Model_System_Config_Source_Product
{
    /**
     * @return array
     */
    public function toOptionArray()
    {

        $options = array();
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->addAttributeToSelect('name');

        $options[] = array(
            'label' => Mage::helper('sample_trade')->__('-- Please Select a Product --'),
            'value' => ''
        );

        foreach ($collection as $product) {
            $options[] = array(
                'label' => sprintf('[%s] %s', $product->getSku(), $product->getName()),
                'value' => $product->getId()
            );
        }


        return $options;
    }
}



















