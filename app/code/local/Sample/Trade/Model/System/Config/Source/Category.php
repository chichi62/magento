<?php


/**
 * Class Sample_Trade_Model_System_Config_Source_Category
 */
class Sample_Trade_Model_System_Config_Source_Category
{
    /**
     * @return array
     */
    public function toOptionArray()
    {

        $options = array();
        $collection = Mage::getResourceModel('catalog/category_collection');

        $collection->addAttributeToSelect('name')
            ->load();

        $options[] = array(
            'label' => Mage::helper('sample_trade')->__('-- Please Select a Category --'),
            'value' => ''
        );

        foreach ($collection as $category) {
            $options[] = array(
                'label' => $category->getName(),
                'value' => $category->getId()
            );
        }

        return $options;
    }
}



















