<?php

/**
 * Class Sample_Trade_Helper_Data
 */
class Sample_Trade_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Configuration path to commercial operation
     */
    const XML_PATH_TRADE_SPECIFIC_GENERAL_PRODUCT = 'specifique/general/product';

    const XML_PATH_TRADE_SPECIFIC_GENERAL_CATEGORY = 'specifique/general/category';

    const XML_PATH_TRADE_SPECIFIC_GENERAL_ACTIVE = 'specifique/general/active';

    const XML_PATH_TRADE_SPECIFIC_GENERAL_AMOUNT = 'specifique/general/amount';

    const XML_PATH_TRADE_SPECIFIC_GENERAL_CART = 'specifique/general/cart';


    /**
     * @return mixed
     */
    public function getProduct()
    {
        return Mage::getModel('catalog/product')->load($this->getProductId());
    }

    public function getProductId($storeId = false)
    {
        return Mage::getStoreConfig(self::XML_PATH_TRADE_SPECIFIC_GENERAL_PRODUCT, $storeId);
    }

    /**
     * @return mixed
     */
    public function getCategory($storeId = false)
    {
        return Mage::getStoreConfig(self::XML_PATH_TRADE_SPECIFIC_GENERAL_CATEGORY, $storeId);
    }

    /**
     * @return mixed
     */
    public function isEnable($storeId = false) //isEnabled
    {
        return Mage::getStoreConfig(self::XML_PATH_TRADE_SPECIFIC_GENERAL_ACTIVE, $storeId);
    }

    /**
     * @return mixed
     */
    public function getNewAmountProduct($storeId = false)
    {
        return Mage::getStoreConfig(self::XML_PATH_TRADE_SPECIFIC_GENERAL_AMOUNT, $storeId);
    }

    /**
     * @return mixed
     */
    public function getAmount($storeId = false)
    {
        return Mage::getStoreConfig(self::XML_PATH_TRADE_SPECIFIC_GENERAL_CART, $storeId);
    }



}
