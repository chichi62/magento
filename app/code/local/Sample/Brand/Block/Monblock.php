<?php

class Sample_Brand_Block_Monblock extends Mage_Core_Block_Template
{
    public function getBrand()
    {
        return Mage::registry('brandId');
    }

    public function getCollectionProducts()
    {
        $brandId = $this->getBrand();

        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('provider', $brandId);
        return $collection;
    }
}