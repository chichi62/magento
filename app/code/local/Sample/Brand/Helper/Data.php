<?php

class Sample_Brand_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function checkIdentifier($brandName){
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'provider');
        /* @var $attribute Mage_Eav_Model_Entity_Attribute_Abstract */

        $optionId = $attribute->getSource()->getOptionId($brandName);
        if($optionId){
            return $optionId;
        }else{
            return false;
        }

    }

}
