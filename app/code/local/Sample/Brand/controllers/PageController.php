<?php


class Sample_Brand_PageController extends Mage_Core_Controller_Front_Action
{

    public function viewAction()
    {
        $brandId = $this->getRequest()->getParam('brand');

        Mage::register('brandId', $brandId);

        $this->loadLayout();
        $this->renderLayout();
    }
}
