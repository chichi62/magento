<?php

class Sample_Brand_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    /**
     * Initialize Controller Router
     *
     * @param Varien_Event_Observer $observer
     */
    public function initControllerRouters($observer)
    {
        /* @var $front Mage_Core_Controller_Varien_Front */
        $front = $observer->getEvent()->getFront();

        $front->addRouter('brand', $this);
    }

    /**
     * Validate and Match controller Page and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {
        $identifier = trim($request->getPathInfo(), '/');

        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }

        $condition = new Varien_Object(array(
            'identifier' => $identifier,
            'continue' => true
        ));

        Mage::dispatchEvent('brand_controller_router_match_before', array(
            'router' => $this,
            'condition' => $condition
        ));

        $identifier = $condition->getIdentifier();

        if ($condition->getRedirectUrl()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect($condition->getRedirectUrl())
                ->sendResponse();
            $request->setDispatched(true);
            return true;
        }

        if (!$condition->getContinue()) {
            return false;
        }


        if (preg_match("#^s/([\w\-]+)\.html$#", $identifier, $brandName)) {
            $brandName = strtoupper($brandName[1]);
        }

        if(!isset($brandName[1])){
            return false;
        }

        $helperBrand = Mage::helper('brand');
        $brandId = $helperBrand->checkIdentifier($brandName);

        if(!$brandId){
            // page 404
            return false;
        }

        $request->setModuleName('brand')
            ->setControllerName('page')
            ->setActionName('view')
            ->setParam('brand', $brandId);

        return true;
    }
}
