<?php

/**
 * Class Sample_Contact_Block_Message
 */
class Sample_Contact_Block_Message extends Mage_Core_Block_Template
{
    /**
     * @return bool
     */
    public function isWeekend()
    {
        return date('N') != 6 && date('N') != 7;
    }

}