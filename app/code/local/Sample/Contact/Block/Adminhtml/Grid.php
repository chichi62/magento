<?php

class Sample_Contact_Block_Adminhtml_Grid extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct()
    {
        $this->_controller = 'adminhtml_contact';
        $this->_blockGroup = 'sample_contact';

        $this->_headerText = 'Manage my contact';
        $this->_addButtonLabel = 'Add a contact';

        parent::__construct();
    }
}
