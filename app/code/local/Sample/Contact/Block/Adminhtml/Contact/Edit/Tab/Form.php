<?php

class Sample_Contact_Block_Adminhtml_Contact_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('contact_form',
            array('legend'=>'ref information'));

        $fieldset->addField('name', 'text',
            array(
                'label' => 'Email',
                'class' => 'required-entry validate-email',
                'required' => true,
                'name' => 'email',
            ));

        $fieldset->addField('message', 'text',
            array(
                'label' => 'Message',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'message',
            ));

        if ( Mage::registry('contact_data') )
        {
            $form->setValues(Mage::registry('contact_data')->getData());
        }

        return parent::_prepareForm();
    }
}

