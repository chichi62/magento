<?php

class Sample_Contact_Block_Adminhtml_Contact_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'sample_contact';
        $this->_controller = 'adminhtml_contact';
        $this->_updateButton('save', 'label', 'Save contact');
        $this->_updateButton('delete', 'label', 'Delete contact');
    }


    public function getHeaderText()
    {
        if (Mage::registry('contact_data') && Mage::registry('contact_data')->getId()) {
            return 'Editer le contact ' . $this->htmlEscape(Mage::registry('contact_data')->getTitle());
        } else {
            return 'Ajouter un contact';
        }
    }
}