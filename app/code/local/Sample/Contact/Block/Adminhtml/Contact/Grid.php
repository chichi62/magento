<?php

class Sample_Contact_Block_Adminhtml_Contact_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('contactGrid');
        $this->setDefaultSort('contact_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sample_contact/contact')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function _prepareColumns()
    {
        $this->addColumn('contact_id', array(
            'header' => 'ID',
            'align' => 'right',
            'width' => '50px',
            'index' => 'contact_id'
        ));


        $this->addColumn('email', array(
            'header' => 'Email',
            'align' => 'left',
            'index' => 'email'
        ));

        $this->addColumn('message', array(
            'header' => 'Message',
            'align' => 'left',
            'index' => 'message'
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }


}