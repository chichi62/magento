<?php
class Sample_Contact_Block_Adminhtml_Contact_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('contact_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle('Information sur le contact');
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => 'About the contact',
            'title' => 'About the contact',
            'content' => $this->getLayout()
                ->createBlock('sample_contact/adminhtml_contact_edit_tab_form')
                ->toHtml()
        ));

        return parent::_beforeToHtml();
    }
}