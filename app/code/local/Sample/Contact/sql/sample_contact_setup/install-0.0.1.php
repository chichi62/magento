<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table 'sample_contact/contact'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('sample_contact/contact'))
    ->addColumn('contact_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Contact ID')
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR, 80, array(
        'nullable'  => false,
        'default'   => null,
    ), 'Contact email')
    ->addColumn('message', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        'nullable'  => false,
        'default'   => null,
    ), 'Contact message')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => true,
        'default'   => '',
    ), 'Value')
    ->setComment('Sample Contact');
$installer->getConnection()->createTable($table);

$installer->endSetup();
