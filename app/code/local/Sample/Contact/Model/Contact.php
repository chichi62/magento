<?php
/**
 * Created by PhpStorm.
 * User: jkelle
 * Date: 02/11/17
 * Time: 16:13
 */

class Sample_Contact_Model_Contact extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('sample_contact/contact');
    }
}