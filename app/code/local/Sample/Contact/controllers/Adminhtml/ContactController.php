<?php

class Sample_Contact_Adminhtml_ContactController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('contact/set_time')->_addBreadcrumb('Contact Manager','Contact Manager');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $filmsId = $this->getRequest()->getParam('id');
        $filmsModel = Mage::getModel('sample_contact/contact')->load($filmsId);

        if ($filmsModel->getId() || $filmsId == 0)
        {
            Mage::register('contact_data', $filmsModel);
            $this->loadLayout();
            $this->_setActiveMenu('films/set_time');
            $this->_addBreadcrumb('contact Manager', 'contact Manager');
            $this->_addBreadcrumb('contact Description', 'contact Description');
            $this->getLayout()->getBlock('head')
                ->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()
                ->createBlock('sample_contact/adminhtml_contact_edit'))
                ->_addLeft($this->getLayout()
                    ->createBlock('sample_contact/adminhtml_contact_edit_tabs')
                );
            $this->renderLayout();
        }
        else
        {
            Mage::getSingleton('adminhtml/session')->addError('contact does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($this->getRequest()->isPost())
        {
            try {
                $postData = $this->getRequest()->getPost();
                if( $this->getRequest()->getParam('id') <= 0 ) {
                    $filmsModel = Mage::getModel('sample_contact/contact')->load($this->getRequest()->getParam('id'));
                }else {
                    $filmsModel = Mage::getModel('sample_contact/contact');
                }

                $filmsModel
                    ->addData($postData)
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess('successfully saved');
                Mage::getSingleton('adminhtml/session')->setcontactData(false);
                $this->_redirect('*/*/');
                return;


            } catch (Exception $e) {

                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setcontactData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

            $this->_redirect('*/*/');
        }
    }

    public function deleteAction()
    {
        if($this->getRequest()->getParam('id') > 0)
        {
            try
            {
                $filmsModel = Mage::getModel('sample_contact/contact');
                $filmsModel->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess('successfully deleted');
                $this->_redirect('*/*/');
            }
            catch (Exception $e)
            {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
}