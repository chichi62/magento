<?php

/**
 * Class Sample_Contact_IndexController
 */
class Sample_Contact_IndexController extends Mage_Core_Controller_Front_Action
{

    /**
     *
     */
    public function indexAction()
    {
        $this->loadLayout();

        if($this->getRequest()->isPost()){
            $data = $this->getRequest()->getParams();
            $email = $data['email'];
            $message = $data['message'];
            $subject = "nouveau message";
            $to = "nicolas.verhoye@gmail.com";

            $contact = Mage::getModel('sample_contact/contact');
            $contact->addData($data);
            $contact->save();

            //if(mail($to,$subject,$message)){
                $this->_redirectUrl('contact/index/confirm');
            //}

        }


        $this->renderLayout();
    }

    public function confirmAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}