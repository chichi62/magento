#!/bin/bash
USER_ID=${DOCKER_USER_ID:-1000}
GROUP_ID=${DOCKER_GROUP_ID:-1000}

deluser docker-user 2> /dev/null
groupadd -g $GROUP_ID docker-user 2> /dev/null
useradd --shell /bin/bash -u $USER_ID -g $GROUP_ID -o -c "" -m docker-user 2> /dev/null
chown -R docker-user:docker-user /home/docker-user

# Apache gets grumpy about PID files pre-existing
# rm -f /var/run/apache2/apache2.pid

exec "$@"
